<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="icon" type="image/jpeg" href="images/Patas_dadas_favicon.jpg"/>
	<title>Patas Dadas</title>
	<link rel="stylesheet" href="css/style.css" type="text/css" rel="stylesheet" />
  <link rel="stylesheet" href="css/bootstrap.css"  type="text/css" rel="stylesheet" />
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/script.js"></script>
</head>
<body>
    <div class="preload">
      <img src="images/teste_patas_menor.gif">
    </div>

    <div id="content">
    <div id="navigation">
      <div class="arrows">
        <span class="ar-left"></span>
        <span class="ar-right"></span>
        <span class="ar-left2"></span>
        <span class="ar-right2"></span>
      </div> 

      <div class="dark-color">
        <div class="light-color">
          <a href="#" id="logo"><img src="images/logo_nav.png"></a>
          <nav>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">Quem Somos</a></li>
              <li><a href="#">Ações</a></li>
              <li><a href="#">Voluntariado</a></li>
              <li><a href="#">As estrelas</a></li>
              <li><a href="#">Faça sua Doação</a></li>
              <li><a href="#">Contate-nos</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
    <div id="banner">
        <div id="banner_patas" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#banner_patas" data-slide-to="0"  class="active"></li>
                <li data-target="#banner_patas" data-slide-to="1"></li>
                <li data-target="#banner_patas" data-slide-to="2"></li>
                <li data-target="#banner_patas" data-slide-to="3"></li>
              </ol>

              <div class="carousel-inner" role="listbox">

                <div class="item  active">
                      <img src="images/img_logo.jpg" alt="Foto 1">
                    <div class="carousel-caption">
                      <h3>Bem-Vindo</h3>
                    </div>
                </div>

                <div class="item">
                      <img src="images/imag_2.jpg" alt="Foto 2">
                    <div class="carousel-caption">
                      <h3>Consuelo</h3>
                    </div>
                </div>

                <div class="item">
                      <img src="images/imag_3.jpg" alt="Foto 3">
                    <div class="carousel-caption">
                      <h3>Polar</h3>
                      <p>Uma diva</p>
                    </div>
                </div>

                <div class="item">
                      <img src="images/imag_1.jpg" alt="Foto 4">
                    <div class="carousel-caption">
                      <h3>Surfista e seus amigos</h3>
                    </div>
                </div>
              </div>

              <a class="left carousel-control" href="#banner_patas" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#banner_patas" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
          </div>
    </div>
    <div id="main_panel">
        <div id="left_show_t">
          Banner 1
        </div>
        <div id="right_show_t">
          Banner 2
        </div>
        <div id="left_show_b">
          Banner 3
        </div>
        <div id="right_show_b">
          Banner 4
        </div>
        <div id="events">
          Rodape
        </div>
    </div>
    </div>
</body>
</html>